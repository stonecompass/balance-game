﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;

public class PlayerConnectionController : MonoBehaviour 
{
	private bool playing;

	[SerializeField]
	private GameObject overlay;

	[SerializeField]
	private Transform player1;
	
	[SerializeField]
	private Transform player2;

	[SerializeField]
	private Text timerText;

	[SerializeField]
	private LineRenderer lineRenderer;

	[SerializeField]
	private Animator animator;

	[SerializeField]
	private float maximumDistance;
	
	[SerializeField]
	private float decreaseDegreeNotConnected;

	[SerializeField]
	private float decreaseDegreeConnected;

	private float timeInSeconds;
	private float decreaseDegree;
	private bool energyLineIsOn;

	void Update () 
	{
		if(player1.localScale.x <= 0.05f || player2.localScale.x <= 0.05f || player1.position.y <= -6 || player2.position.y <= -6)
		{
			overlay.SetActive(true);
			GameManager.Playing = false;
			animator.SetBool("animate", true);
		}

		if(GameManager.Playing)
		{
			timeInSeconds += Time.deltaTime;
			timerText.text = timeInSeconds.ToString("0.##");

			if(Vector3.Distance(player1.position, player2.position) > maximumDistance)
			{
				decreaseDegree = decreaseDegreeNotConnected;
				lineRenderer.gameObject.SetActive(false);
				energyLineIsOn = false;
			}
			else
			{
				decreaseDegree = decreaseDegreeConnected;
				lineRenderer.SetPositions(new Vector3[] { player1.position, player2.position });	
				lineRenderer.gameObject.SetActive(true);
				energyLineIsOn = true;	
			}

			player1.localScale = new Vector3(player1.localScale.x - decreaseDegree / 100 * Time.deltaTime, player1.localScale.y - decreaseDegree / 100 * Time.deltaTime, 1);
			player2.localScale = new Vector3(player2.localScale.x - decreaseDegree / 100 * Time.deltaTime, player2.localScale.y - decreaseDegree / 100 * Time.deltaTime, 1);
		}
	}

	public void StartGiveEnergy(int toPlayerNumber)
	{
		StartCoroutine(GiveEnergy(toPlayerNumber));
	}

	IEnumerator GiveEnergy(int playerNumber)
	{
		if(playerNumber == 0)
		{
			player1.GetComponent<PlayerController>().PlaySound();
			player1.GetComponent<PlayerController>().Pulsate();
		}
		else
		{
			player2.GetComponent<PlayerController>().PlaySound();
			player2.GetComponent<PlayerController>().Pulsate();
		}

		float pumped = 0.0f;
		while(pumped < 0.04f)
		{
			yield return new WaitForSeconds(0.05f);

			if(playerNumber == 0)
			{
				player1.localScale = new Vector3(player1.localScale.x + 0.001f, player1.localScale.y + 0.001f, 1);
			}
			else
				player2.localScale = new Vector3(player2.localScale.x + 0.001f, player2.localScale.y + 0.001f, 1);

			pumped += 0.001f;
		}
	}

	public void StartPumpEnergy(int fromPlayerNumber)
	{
		if(energyLineIsOn)
		{
			if(fromPlayerNumber == 0)
				StartCoroutine(PumpEnergy(1));
			else
				StartCoroutine(PumpEnergy(0));
		}
	}

	IEnumerator PumpEnergy(int playerNumber)
	{
		float startWidth = 0.1f;
		float pumped = 0.0f;
		while(pumped < 0.01f)
		{
			yield return new WaitForSeconds(0.1f);
			
			if(!energyLineIsOn)
				break;

			if(playerNumber == 0)
			{
				lineRenderer.startWidth = 0.4f;
				lineRenderer.endWidth = startWidth;
				player1.localScale = new Vector3(player1.localScale.x + 0.001f, player1.localScale.y + 0.001f, 1);
				player2.localScale = new Vector3(player2.localScale.x - 0.001f, player2.localScale.y - 0.001f, 1);
			}
			else
			{

				lineRenderer.startWidth = startWidth;
				lineRenderer.endWidth = 0.4f;
				player2.localScale = new Vector3(player2.localScale.x + 0.001f, player2.localScale.y + 0.001f, 1);
				player1.localScale = new Vector3(player1.localScale.x - 0.001f, player1.localScale.y - 0.001f, 1);
			}
			pumped += 0.001f;
		}
		lineRenderer.startWidth = startWidth;
		lineRenderer.endWidth = startWidth;
	}
}
