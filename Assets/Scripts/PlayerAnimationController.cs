﻿using UnityEngine;

public class PlayerAnimationController : MonoBehaviour 
{
	public void StopPulsating()
	{
		GetComponent<Animator>().SetBool("pulsating", false);
	}
}
