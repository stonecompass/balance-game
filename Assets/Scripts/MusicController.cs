﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicController : MonoBehaviour 
{
	private static MusicController instance;

	void Start () 
	{
		if(instance == null)
			instance = this;
			
		if(instance != this)
			Destroy(gameObject);

		DontDestroyOnLoad(gameObject);
	}

}
