﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyController : MonoBehaviour 
{
	public PlayerConnectionController playerConnectionController;

	void OnTriggerEnter2D(Collider2D collider)
	{
		if(collider.tag == "Player")
		{
			playerConnectionController.StartGiveEnergy(collider.GetComponent<PlayerController>().PlayerNumber);
			gameObject.SetActive(false);
		}
	}
}
