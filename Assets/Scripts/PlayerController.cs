﻿using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine;

public struct PlayerControlKeys
{
	public KeyCode Up;
	public KeyCode Down;
	public KeyCode Left;
	public KeyCode Right;
	public KeyCode PumpEnergy;
}

public class PlayerController : MonoBehaviour 
{
	private PlayerControlKeys[] playerControls = 
	{ 
		new PlayerControlKeys { Up = KeyCode.W, Down = KeyCode.S, Left = KeyCode.A, Right = KeyCode.D, PumpEnergy = KeyCode.Space }, 
		new PlayerControlKeys { Up = KeyCode.UpArrow, Down = KeyCode.DownArrow, Left = KeyCode.LeftArrow, Right = KeyCode.RightArrow, PumpEnergy = KeyCode.RightControl } 
	};

	[SerializeField]
	private PlayerConnectionController playerConnectionController;

	public int PlayerNumber { get { return playerNumber; }}
	[SerializeField]
	private int playerNumber;

	[SerializeField]
	private float movementSpeed;

	[SerializeField]
	private AudioSource collectSound;

	private Rigidbody2D rigidbody;

	private float velocityX;
	private float velocityY;

	private bool goingInDirection;

	void Start () 
	{
		rigidbody = GetComponent<Rigidbody2D>();	
	}
	
	void Update () 
	{
		velocityX = 0;
		velocityY = 0;

		if(GameManager.Playing)
		{
			if(Input.GetKey(playerControls[playerNumber].Up))
				velocityY = movementSpeed;
			else if(Input.GetKey(playerControls[playerNumber].Down))
				velocityY = -movementSpeed;

			if(Input.GetKey(playerControls[playerNumber].Left))
				velocityX = -movementSpeed;
			else if(Input.GetKey(playerControls[playerNumber].Right))
				velocityX = movementSpeed;

			if(Input.GetKeyDown(playerControls[playerNumber].PumpEnergy))
				playerConnectionController.StartPumpEnergy(playerNumber);
		}
		else
		{
			if(Input.GetKeyDown(KeyCode.Return))
			{
				SceneManager.LoadScene("PlayScene");
				GameManager.Playing = true;
			}
		}
					
		if(!goingInDirection)
			rigidbody.velocity = new Vector2(velocityX, velocityY);
	}

	public void PlaySound()
	{
		collectSound.pitch = Random.Range(1 - 0.1f, 1 + 0.1f);
		collectSound.Play();
	}

	public void GoInDirection(Vector3 direction)
	{
		goingInDirection = true;
		rigidbody.AddForce(direction * 5000);
		StartCoroutine(StopCooldown());
	}

	IEnumerator StopCooldown()
	{
		yield return new WaitForSeconds(0.35f);
		goingInDirection = false;
	}

	public void Pulsate()
	{
		GetComponent<Animator>().SetBool("pulsating", true);
	}
}
